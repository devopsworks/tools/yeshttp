FROM devopsworks/golang-upx:1.24 as builder

ARG version
ARG builddate

WORKDIR /src

# COPY go.mod .
# COPY go.sum .

# RUN go mod download

COPY . .

# hadolint ignore=SC2097,SC2098
ENV GOOS=linux \
    GOARCH=amd64 \
    CGO_ENABLED=0

RUN go build \
        -ldflags "-X main.Version=${version} -X main.BuildDate=${builddate}" \
        -a \
    -o /go/bin/yeshttp cmd/yeshttp.go && \
    strip /go/bin/yeshttp && \
    /usr/local/bin/upx -9 /go/bin/yeshttp

# hadolint ignore=DL3006
FROM gcr.io/distroless/base-debian10

COPY --from=builder /go/bin/yeshttp /usr/local/bin/yeshttp

ENTRYPOINT ["/usr/local/bin/yeshttp", "-bind", "0.0.0.0"]
