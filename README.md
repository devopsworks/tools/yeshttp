# yeshttp

A web server that always says yes.

`yeshttp` listens to HTTP requests and dumps the request in the terminal. This
tool is handy to debug HTTP clients.

## Usage

- `-bind <string>`: Bind address (default "127.0.0.1")
- `-port <string>`: Bind port (default "8888")
- `-status <int>`: Status code to respond to requests (default 200)

## Build

```
make
```

## Example run

```
$ ./bin/yeshttp -port 1234 -status 201
listening on 127.0.0.1:1234
# (in another terminal: curl -XPOST  localhost:1234/foo/bar
* POST /foo/bar requested from 127.0.0.1:45518
User-Agent: curl/7.68.0
Accept: */*

```

## Licence

MIT
