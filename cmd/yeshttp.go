package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/lestrrat-go/jwx/jwt"
	"github.com/mileusna/useragent"
)

var (
	// Version of binary
	Version string
	// BuildDate of binary
	BuildDate string
	status    int
)

func main() {
	var (
		bind    string
		port    string
		version bool
	)

	flag.StringVar(&bind, "bind", "127.0.0.1", "Bind address")
	flag.StringVar(&port, "port", "8888", "Bind port")
	flag.IntVar(&status, "status", 200, "Status code to respond to requests")
	flag.BoolVar(&version, "version", false, "Version")
	flag.Parse()

	if version {
		fmt.Printf("version %s built %s\n", Version, BuildDate)
		os.Exit(0)
	}
	listen := fmt.Sprintf("%s:%s", bind, port)
	fmt.Printf("listening on %s\n", listen)
	http.HandleFunc("/", handleRequest)
	log.Fatal(http.ListenAndServe(listen, nil))
}

func handleRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(status)
	w.Write([]byte("<html><head><title>yeshttp</title></head><body>\n"))
	w.Write([]byte("<style>body { font-family: monospace; }</style>\n"))
	defer w.Write([]byte("</body></html>\n"))

	color.New(color.FgHiGreen).Printf("* %s %s requested from %s at %s\n", r.Method, r.URL, r.RemoteAddr, time.Now().Format(time.RFC3339))
	for k, v := range r.Header {
		for _, inner := range v {
			fmt.Printf("%s: %s\n", color.HiCyanString(k), inner)
			w.Write([]byte(fmt.Sprintf("<b><a href=https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/%s target=\"_blank\">%s</a></b>: %s<br/>\n", k, k, inner)))
		}
		if k == "Authorization" {
			parseJWT(w, v[0])
		}
		if k == "User-Agent" {
			parseUserAgent(w, v[0])
		}
	}
	defer r.Body.Close()
	buf, err := io.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(os.Stderr, "%v", err)
	}

	if len(buf) > 0 {
		fmt.Printf("%s:\n", color.HiYellowString("Body"))
		fmt.Println(string(buf))
		w.Write([]byte("<b>Body</b>:<br/>\n"))
		w.Write([]byte(fmt.Sprintf("%s<br/>\n", string(buf))))
	} else {
		fmt.Printf("%s: <nil>\n", color.HiYellowString("Body"))
		w.Write([]byte("<b>Body</b>: &lt;nil&gt;<br/>\n"))
	}
}

func parseJWT(w http.ResponseWriter, field string) {
	t := strings.Split(field, " ")[1]
	token, err := jwt.Parse([]byte(t))
	if err != nil {
		fmt.Printf("failed to parse JWT: %s\n", err)
		w.Write([]byte(fmt.Sprintf("failed to parse JWT: %s<br/>\n", err)))
		return
	}

	fmt.Printf("  %s: %s\n", color.HiBlueString("aud"), token.Audience())
	fmt.Printf("  %s: %s\n", color.HiBlueString("exp"), token.Expiration())
	fmt.Printf("  %s: %s\n", color.HiBlueString("iss"), token.Issuer())
	fmt.Printf("  %s: %s\n", color.HiBlueString("jti"), token.JwtID())
	fmt.Printf("  %s: %s\n", color.HiBlueString("nbf"), token.NotBefore())
	fmt.Printf("  %s: %s\n", color.HiBlueString("sub"), token.Subject())
	w.Write([]byte("<ul>\n"))
	w.Write([]byte(fmt.Sprintf("<li><b>aud</b>: %s</li>\n", token.Audience())))
	w.Write([]byte(fmt.Sprintf("<li><b>exp</b>: %s</li>\n", token.Expiration())))
	w.Write([]byte(fmt.Sprintf("<li><b>iss</b>: %s</li>\n", token.Issuer())))
	w.Write([]byte(fmt.Sprintf("<li><b>jti</b>: %s</li>\n", token.JwtID())))
	w.Write([]byte(fmt.Sprintf("<li><b>nbf</b>: %s</li>\n", token.NotBefore())))
	w.Write([]byte(fmt.Sprintf("<li><b>sub</b>: %s</li>\n", token.Subject())))

	for k, v := range token.PrivateClaims() {
		fmt.Printf("  %s: %v\n", color.HiBlueString(k), v)
		w.Write([]byte(fmt.Sprintf("<li><b>%s</b>: %v</li>\n", k, v)))
	}
	w.Write([]byte("</ul>\n"))
}

func parseUserAgent(w http.ResponseWriter, field string) {
	ua := useragent.Parse(field)

	device := ua.Device
	if device == "" {
		device = "Unknown"
	}

	platform := "Unknown"
	switch {
	case ua.Mobile:
		platform = "Mobile"
	case ua.Tablet:
		platform = "Tablet"
	case ua.Desktop:
		platform = "Desktop"
	case ua.Bot:
		platform = "Bot"
	default:
		platform = "Unknown"
	}

	fmt.Printf("  %s: %s\n", color.HiBlueString("Browser"), ua.Name)
	fmt.Printf("  %s: %s\n", color.HiBlueString("Version"), ua.Version)
	fmt.Printf("  %s: %s (%s)\n", color.HiBlueString("OS"), ua.OS, ua.OSVersion)
	fmt.Printf("  %s: %s\n", color.HiBlueString("Device"), device)
	fmt.Printf("  %s: %s\n", color.HiBlueString("Platform"), platform)

	w.Write([]byte("<ul>\n"))
	w.Write([]byte(fmt.Sprintf("<li><b>Browser</b>: %s</li>\n", ua.Name)))
	w.Write([]byte(fmt.Sprintf("<li><b>Version</b>: %s</li>\n", ua.Version)))
	w.Write([]byte(fmt.Sprintf("<li><b>OS</b>: %s (%s)</li>\n", ua.OS, ua.OSVersion)))
	w.Write([]byte(fmt.Sprintf("<li><b>Device</b>: %s</li>\n", device)))
	w.Write([]byte(fmt.Sprintf("<li><b>Platform</b>: %s</li>\n", platform)))
	w.Write([]byte("</ul>\n"))
}
